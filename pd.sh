#!/bin/sh

if [ $# -ne 2 ]; then
    echo "Illegal number of parameters. Usage: pd.sh <package id> <filename>"
    exit 1
fi

cd /sdcard/Download/pixeldungeon/

UID=`dumpsys package $1 | grep userId= | awk '{split($0, a, "="); print a[2]}'`

am force-stop $1

if [ -f out.json ]
then
    echo "Copying game data back in..."
    rm data.json
    gzip out.json
    mv out.json.gz /data/data/$1/files/$2.dat
else
    echo "Copying game data out..."
    cp /data/data/$1/files/$2.dat $2.dat
    cp $2.dat $2_$(date +%Y-%m-%d-%H-%M-%S).dat
    gunzip -fck $2.dat > data.json
fi

chown -R $UID:$UID /data/data/$1/files/
