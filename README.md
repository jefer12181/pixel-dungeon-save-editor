Pixel Dungeon Savegame Editor
=============================

Pixel Dungeon savegame editor is a small Python script that will help you edit your PD saves.

Prerequisites
-------------

You need a rooted phone and adb installed on your computer. The script is meant to run on Linux and/or OS X, it probably
won't work on Windows.


Usage
-----

To run, simply connect your Android phone to your computer, allow debugging through ADB, and run the pixeldungeon.py
script. The script will open an editor with the data, just edit whatever you want, save and quit. The data will be
copied back to the device.

The script will edit Shattered Pixel Dungeon by default. To edit some other game, just pass the package name in the
command line:

```bash
$ ./pixeldungeon.py com.watabou.pixeldungeon
```
