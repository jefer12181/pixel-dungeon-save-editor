#!/usr/bin/env python3

import datetime
import json
import os
import sys

FILENAME = "/tmp/out-%s.json" % datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S")
TEMPDIR = "/storage/emulated/0/Download/pixeldungeon"

if len(sys.argv) > 1:
    PACKAGENAME = sys.argv[1]
    CHARNAME = sys.argv[2]
else:
    print("Running with defaults. To override, run like so:")
    print("./pixeldungeon.py com.shatteredpixel.shatteredpixeldungeon ranger")
    PACKAGENAME = "com.shatteredpixel.shatteredpixeldungeon"
    CHARNAME = "ranger"


def get_item(data, item_name):
    for item in data["hero"]["inventory"]:
        if item_name in item["__className"]:
            return item


# Push the pd.sh script to the device.
os.system("adb shell su -c mkdir %s/" % (TEMPDIR, ))
os.system("adb push pd.sh %s/pd.sh" % (TEMPDIR, ))

# Copy the game data out.
os.system("adb shell su -c sh %s/pd.sh %s %s" % (TEMPDIR, PACKAGENAME, CHARNAME))

# Pull the data to this computer.
os.system("adb pull %s/data.json %s" % (TEMPDIR, FILENAME))

# Read the data.
with open(FILENAME) as infile:
    data = json.load(infile)

# Give full health.
data["hero"]["HP"] = data["hero"]["HT"]

# Identify everything.
for key, value in data.items():
    if ("_known" not in key) or value:
        continue
    data[key] = True

# Give lots of food.
item = get_item(data, "food.Food")
if item:
    item["quantity"] = 200
else:
    data["hero"]["inventory"].append({
        "__className": PACKAGENAME + ".items.food.Food",
        "quantity": 916,
        "level": 0,
        "levelKnown": True,
        "cursed": False,
        "cursedKnown": True
    })

# Give an Ankh.
item = get_item(data, "items.Ankh")
if item:
    item["blessed"] = True
else:
    data["hero"]["inventory"].append({
        "__className": PACKAGENAME + ".items.Ankh",
        "quantity": 1,
        "level": 0,
        "levelKnown": False,
        "cursed": False,
        "cursedKnown": True,
        "blessed": True
    })

# Give a bandolier.
if not get_item(data, "items.bags.PotionBandolier"):
    data["hero"]["inventory"].append({
        "__className": PACKAGENAME + ".items.bags.PotionBandolier",
        "quantity": 1,
        "level": 0,
        "levelKnown": False,
        "cursed": False,
        "cursedKnown": True,
        "inventory": []
    })

# Give a scroll holder.
if not get_item(data, "items.bags.ScrollHolder"):
    data["hero"]["inventory"].append({
        "__className": PACKAGENAME + ".items.bags.ScrollHolder",
        "quantity": 1,
        "level": 0,
        "levelKnown": False,
        "cursed": False,
        "cursedKnown": False,
        "inventory": []
    })

# Give a wand holster.
if not get_item(data, "items.bags.WandHolster"):
    data["hero"]["inventory"].append({
        "__className": PACKAGENAME + ".items.bags.WandHolster",
        "quantity": 1,
        "level": 0,
        "levelKnown": False,
        "cursed": False,
        "cursedKnown": False,
        "inventory": []
    })

# Give a seed pouch.
if not get_item(data, "items.bags.SeedPouch"):
    data["hero"]["inventory"].append({
        "__className": PACKAGENAME + ".items.bags.SeedPouch",
        "quantity": 1,
        "level": 0,
        "levelKnown": False,
        "cursed": False,
        "cursedKnown": False,
        "inventory": []
    })

for item in data["hero"]["inventory"]:
    # Show curses.
    item["cursedKnown"] = True

# Write a pretty-printed dump of the data that we can edit.
with open(FILENAME, "w") as outfile:
    json.dump(data, outfile, indent=4, sort_keys=True)

os.system("vim %s" % (FILENAME, ))

# Read the edited data back in.
with open(FILENAME) as infile:
    data = json.load(infile)

# Write the data in a format the game can read.
with open(FILENAME, "w") as outfile:
    json.dump(data, outfile)

# Push the data to the device.
os.system("adb push %s %s/out.json" % (FILENAME, TEMPDIR))

# Copy the data back to the game directory.
os.system("adb shell su -c sh %s/pd.sh %s %s" % (TEMPDIR, PACKAGENAME, CHARNAME))
